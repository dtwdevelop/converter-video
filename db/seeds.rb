# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Genre.create([{name: 'Fantastic', hide: true},{name:'Comedy' ,hide:true}])
Category.create([{name: 'Child'} ,{name: 'Adult'}])
Album.create(name:'Film1',category_id:1,genre_id:1,description:'My first category')
AlbumFile.create(album_id:1,filename:'video.mp4')
# User.create(login:'admin',email:'test@admin.com',password:'demo',role:'Admin',token:rand)

