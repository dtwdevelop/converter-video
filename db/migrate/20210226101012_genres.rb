class Genres < ActiveRecord::Migration[6.1]
  def change
    create_table :genres do |t|
      t.string :name
      t.boolean :hide
      t.timestamps
      end
  end
end
