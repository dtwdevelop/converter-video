class AlbumFiles < ActiveRecord::Migration[6.1]
  def change
    create_table :album_files do |t|
      t.integer :album_id
      t.string  :filename
      t.timestamps
      t.belongs_to :album ,foreign_key: true
    end
  end
end
