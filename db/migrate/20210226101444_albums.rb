class Albums < ActiveRecord::Migration[6.1]
  def change
    create_table :albums do |t|
      t.string :name
      t.integer :category_id
      t.integer :genre_id
      t.text :description
      t.timestamps
    end
  end
end
