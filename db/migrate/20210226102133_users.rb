class Users < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :login
      t.string :email
      t.string :password_digest
      t.string :role
      t.string :token
    end
  end
end
