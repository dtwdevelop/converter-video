Rails.application.routes.draw do
  root  "site#index"
  get "/dashboard" , to: "site#index"
  get "/library" , to: "site#index"
  get "/library/:id" , to: "site#index"
  get "/channels" , to: "site#index"
  get "/settings" , to: "site#index"
  get "/users" , to: "site#index"
  get "/login" , to: "site#index"
  resources :settings

  get '/files' , to:"album#files"
  get '/albums' , to: "album#albums"
  get '/album/:id', to: 'album#one_album'
  get '/get-genres' , to: "settings#genres"
  get '/get-categories' , to: 'settings#categories'
  post '/add-library', to: 'album#add_album'
  post '/convert-video', to: 'album#convert_video'
  get '/loading' , to: "album#loading"
  get '/sample' , to: 'album#sample'
  delete '/remove-album' ,to: 'album#delete_album'
  #settings
  post 'add-genre' , to: 'settings#add_genre'
  delete 'delete-genre' ,to: 'settings#delete_genre'
  post 'add-category', to: 'settings#add_category'
  delete 'delete-category' , to: 'settings#delete_category'
  #users
  get 'all-users', to:'user#user_all'
  post 'create-user', to: 'user#create_user'
  delete 'delete-user', to: 'user#delete_user'
  put 'change-role' , to: 'user#change_role'
  get 'auth', to: 'user#login'
end
