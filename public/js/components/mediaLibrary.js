Vue.use(window.vuelidate.default)
const { required, minLength, sameAs, email } = window.validators
export default {
    name: "MediaLibrary",
    template: `
 <v-container>
 <v-row>
 <v-col>
 <h3>Library</h3>

<v-btn @click="dialog2=true" small>Add</v-btn>
 </v-col>
 </v-row>

 <template>
  <v-row justify="center">

 <v-dialog v-model="dialog" max-width="290">
      <v-card>
        <v-card-title class="headline">
         Source
        </v-card-title>
<v-card-text>
         <v-checkbox v-model="step" label="Тип"></v-checkbox>
<v-file-input v-if="step" prepend-icon="mdi-file"  :error-messages="fileError" @input="$v.sources.$touch()" @blur="$v.sources.$touch()" chips v-model="sources"  chips multiple label="Источник"></v-file-input>
<v-text-field v-if="!step" v-model="library.sources" label="Название" required ></v-text-field>
</v-card-text>
<v-card-actions>
 <v-spacer></v-spacer>
 <v-btn color="green darken-1"  text @click="dialog = false" >
            Close
          </v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
 </v-row>
</template>

 <v-dialog v-model="dialog2"  max-width="700">
<v-card>
<v-card-title>Description library</v-card-title>
<v-card-text>
<v-form  dense  >
  <v-row>
  <v-col sm="2">
   <v-text-field v-model="library.name" required :error-messages="nameErrors" @input="$v.library.name.$touch()" @blur="$v.library.name.$touch()"  label="Name"  ></v-text-field>
</v-col>
 <v-col sm="2">
 <v-btn color="primary" dark @click.stop="dialog = true">
      Files
    </v-btn>
<span class="danger">{{fileError[0]}}</span>
</v-col>
</v-row>
<v-row>
<v-col sm="2"> <v-select item-text="name" item-value="id"  v-model="library.categories" :items="categories" label="Category" required :error-messages="categoryErrors" @input="$v.library.categories.$touch()" @blur="$v.library.categories.$touch()" ></v-select></v-col>
<v-col sm="2"> <v-select item-text="name" item-value="id"  v-model="library.genres" :items="genres" label="Genres" required :error-messages="genreErrors" @input="$v.library.genres.$touch()" @blur="$v.library.genres.$touch()"></v-select></v-col>
</v-row>
<v-row>
<v-col sm="5" >
 <v-textarea  v-model="library.desc" required :error-messages="descErrors" @input="$v.library.desc.$touch()" @blur="$v.library.desc.$touch()"  color="teal">
  <template v-slot:label> <div>Description </small> </div> </template></v-textarea>
  <v-progress-linear v-model="progress" height="25">
  <strong>{{ Math.ceil(progress) }}%</strong>
</v-progress-linear>
<v-btn color="success" :disabled="error" class="mr-4" @click="add_library">Save</v-btn></v-col>

</v-row>
</v-form>
</v-card-text>
<v-card-actions>
          <v-spacer></v-spacer>
 <v-btn color="green darken-1"  text @click="dialog2 = false" >
  Close
 </v-btn>
 </v-card-actions>
</v-card>
</v-dialog>

<v-data-iterator :items="albums" :single-expand="singleExpand">
<template v-slot:default="{ items ,isExpanded ,expand}">
<v-row>
<v-col v-for="album in items" :key="album.id">
 <v-card  class="mx-auto" max-width="350">
  <v-card-title> {{album.name}} </v-card-title>
<v-btn small link :to="'/library/'+album.id">Video list</v-btn>
  <v-card-text>
</v-card-text>
 <v-card-actions>
<v-spacer></v-spacer>
 <v-btn icon  @click="(v) => expand(album, v)">
        <v-icon>{{ show ? 'mdi-chevron-up' : 'mdi-chevron-down' }}</v-icon>
      </v-btn>
    </v-card-actions>

    <v-expand-transition>
      <div v-show="isExpanded(album)">
        <v-divider></v-divider>
     <v-card-text>
     {{album.description}}
     <v-btn color="red" @click="removeAlbum(album.id)">x</v-btn>
        </v-card-text>
      </div>
    </v-expand-transition>
  </v-card>
  </v-col>
  </v-row>
  </template>
  </v-data-iterator>


</v-container>

    `,
    data: () => ({
        status: null,
        show: false,
        sources : [],
        singleExpand: true,
        library: {
            name : '',
            categories : [],
            genres  : [],
            desc : ''
        },
        dialog: false ,
        dialog2: false,
        step : true,


    }),
    validations :{
        sources :{
            required,
        },
        library:{
            name :{
                required,
                minLength: minLength(4)
            },
            desc:{
                required,
                minLength :minLength(4)
            },
            genres :{
                required
            },
            categories :{
                required
            }
        }
        },
    computed: {
        files : function(){
            return this.$store.state.files;
        },
        genres : function(){
            return  this.$store.state.genres
        },
        categories : function(){
            return this.$store.state.categories
        },
        error : function(){
            return this.$v.library.$anyError
        },
        nameErrors(){
            const errors = []
            if (!this.$v.library.name.$dirty) return errors
            !this.$v.library.name.minLength && errors.push('Длина имени должна быть больше 4 символов')
            !this.$v.library.name.required && errors.push('Имя обязательно.')
            return errors
        },
        descErrors(){
            const errors = []
            if (!this.$v.library.desc.$dirty) return errors
            !this.$v.library.desc.minLength && errors.push('Длина описание должна быть больше 4 символов')
            !this.$v.library.desc.required && errors.push('Описание обязательно.')
            return errors
        },
        genreErrors(){
            const errors = []
            if (!this.$v.library.genres.$dirty) return errors
            !this.$v.library.genres.required && errors.push('Жанр обязательно.')
            return errors
        },
        categoryErrors(){
            const errors = []
            if (!this.$v.library.categories.$dirty) return errors
            !this.$v.library.categories.required && errors.push('Жанр обязательно.')
            return errors
        },
        fileError(){
            const errors = []
            if (!this.$v.sources.$dirty) return errors
            !this.$v.sources.required && errors.push('Файл обязательно.')
            return errors
        },
        albums : function(){
           return this.$store.state.albums
        },
        progress: function(){
           return this.$store.state.progress
        },
    },
    mounted() {
       this.$store.dispatch('get_all_albums')
    },

    created(){
        this.$store.dispatch('get_categories')
        this.$store.dispatch('get_genres')
        this.load_files_library();

    },

    methods: {
        async load_files_library() {
          this.$store.dispatch('load_files')
        },

        add_library(){
            this.$v.$touch()
            if(!this.$v.$invalid){
               this.$store.dispatch('add_library',{data:this.library,files:this.sources}).then(()=>{
                   this.$store.commit('change_progress',{progress:0})
               })
            }
       
        },
        removeAlbum(id){
            if(id != null){
                this.$store.dispatch('remove_album',{album_id:id})
            }
           
        }

    }
}

