export default {

    name: 'Admin',
    template: `
    <div>
    <v-app >

    <v-main>
   <v-toolbar>
<v-toolbar-title><v-icon @click="drawer=!drawer">mdi-view-stream</v-icon></v-toolbar-title>
<v-spacer></v-spacer>
<v-toolbar-items>
<v-btn v-if="status" small @click="logOut">exit</v-btn>
<v-btn to="/dashboard"><v-icon>mdi-view-dashboard</v-icon></v-btn>
</v-toolbar-items>
</v-toolbar>
  <v-navigation-drawer  v-model="drawer" absolute bottom temporary>
  <v-list-item-group>
          <v-list-item>
            <v-list-item-title>Library<v-icon>mdi-newspaper</v-icon></v-list-item-title>
          </v-list-item>

          <v-list-item>
            <v-list-item-title>Users<v-icon>mdi-account</v-icon></v-list-item-title>
          </v-list-item>
          
            <v-list-item>
            <v-list-item-title>Channels<v-icon>mdi-video</v-icon></v-list-item-title>
          </v-list-item>

         
        </v-list-item-group>
      </v-list>
  </v-navigation-drawer>
<v-container>
<router-view></router-view>
 </v-container>
  </v-main>
</v-app>
   </div>`,
    data: () => ({
        sticky: true,
        drawer: false

    }),
    computed: {
        status : function(){
            return this.$store.state.token!=null?true:false
        }

    },

    created() {

    },
    methods: {
      logOut(){
          this.$store.dispatch('logout')
          this.$router.push('/login')
      }

    }
}
