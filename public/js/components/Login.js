Vue.use(window.vuelidate.default)
const { required, minLength, sameAs, email } = window.validators
export default {
    name: 'Login',
    template : `<v-container>
    <v-row>
    <v-col  sm="4">
    <v-form  dense >
    
    <v-text-field v-model="form_login.login" required :error-messages="loginErrors" @input="$v.form_login.login.$touch()" @blur="$v.form_login.login.$touch()"  label="Логин" ></v-text-field>
     <v-text-field v-model="form_login.password" required :error-messages="passwordErrors" @input="$v.form_login.password.$touch()" @blur="$v.form_login.password.$touch()" label="Пароль"  ></v-text-field>
     <v-btn small :disabled="status" @click="loginIn"> Добавить</v-btn>
     </v-form>
     </v-col>
     </v-row>
    
    </v-container>`,
    data:()=>({
        status :false,
      form_login:{
          login : '',
          password : '',
      },
    }),
      validations:{
        form_login:{
             login:{
                 required,
                 minLength: minLength(4)
             },
         password: {
                required,
            },
        }
    },
      computed :{
        loginErrors() {
            const errors = []
            if (!this.$v.form_login.login.$dirty) return errors
            !this.$v.form_login.login.minLength && errors.push('Длина логина должна быть больше 4 символов')
            !this.$v.form_login.login.required && errors.push('Логин обязательно.')
            return errors
        },
        passwordErrors(){
            const errors = []
            if (!this.$v.form_login.password.$dirty) return errors
            !this.$v.form_login.password.required && errors.push('Пароль обязательно.')
            return errors
        },

      },
      methods:{
         async loginIn(){
            this.$v.$touch()
            if(!this.$v.$invalid){
             this.$store.dispatch('login_in',{login:this.form_login.login, password:this.form_login.password}).then((data)=>{
              if(data.login =='ok') this.$router.push('/dashboard')
              
             })
            
            }
              },
         

      }
   
}