export default {
    name : "Dashboard",

    template: `
  <v-container>
  <h3>Dashboard</h3>
   <v-row>
      <v-col>
     <h4> </h4>
       <v-card class="mx-auto" max-width="344">
      <v-card-title>Channels </v-card-title>
      <v-card-text>
      <v-btn link to="/channels"><v-icon x-large>mdi-youtube-tv</v-icon></v-btn>

      </v-card-text>
      <v-card-actions>
      </v-card-actions>
    </v-card>
   </v-col>

   <v-col>

    <v-card class="mx-auto" max-width="344">
    <v-card-title> Library</v-card-title>
    <v-card-text>

    <v-btn link to="/library"> <v-icon x-large>mdi-image-multiple</v-icon></v-btn>

  </v-card-text>
    <v-card-actions>
    </v-card-actions>
  </v-card>
</v-col>
</v-row>

 <v-row>
<v-col>
<v-card class="mx-auto" max-width="344">
<v-card-title>Users</v-card-title>
<v-card-text>
<v-btn link to="/users"><v-icon x-large>mdi-account-supervisor</v-icon></v-btn>
</v-card-text>
<v-card-actions>
</v-card-actions>
</v-card>
</v-col>

<v-col>
<v-card class="mx-auto" max-width="344">
<v-card-title>Settings</v-card-title>
<v-card-text>
<v-btn to="/settings"><v-icon x-large>mdi-settings</v-icon></v-btn>
</v-card-text>
<v-card-actions>
</v-card-actions>
</v-card>

</v-col>
</v-row>
</v-container>

    `,
    data: () => ({
     status: null

    }),
    computed: {


    },
    mounted(){

    },
    methods: {
      UserList()
        {
        }

    }
}

