Vue.use(window.vuelidate.default)
const { required, minLength, sameAs, email } = window.validators
export default {
    name: "Channels",
    template: `
 <v-container>
 <h3>Channels</h3>

<v-row>
<v-col>
<v-data-table :headers="headers" :items="channels" :items-per-page="10" class="elevation-1" ></v-data-table>
 <v-spacer></v-spacer>
</v-col>
</v-row>
<v-row>
<v-col>
</v-col>
</v-row>

</v-container>

    `,
    data: () => ({
        status: null,
        show: false,
        categories : [],
        channels :[],
        headers: [

            { text: 'Logo', value: 'logo' },
            { text: 'Video codec', value: 'video' },
            { text: 'Audio codec', value: 'audio' },
            { text: 'Size', value: 'size' },
            { text: 'frequency', value: 'fps' },
        ],


    }),

    computed: {

    },
    mounted() {

    },
    methods: {}
}

