Vue.use(window.vuelidate.default)
const { required, minLength, sameAs, email } = window.validators
export default {
    name: "User",
    template: `
 <v-container>
    <v-dialog  v-model="dialog" max-width="490">
      <v-card>
        <v-card-title class="headline">
         user
        </v-card-title>
<v-card-text>
<v-form  dense >
<v-text-field v-model="form_user.login" required :error-messages="loginErrors" @input="$v.form_user.login.$touch()" @blur="$v.form_user.login.$touch()"  label="Логин" ></v-text-field>
 <v-text-field v-model="form_user.email" required :error-messages="emailErrors" @input="$v.form_user.email.$touch()" @blur="$v.form_user.email.$touch()"   label="Эмайл" required ></v-text-field>
 <v-text-field v-model="form_user.password" required :error-messages="passwordErrors" @input="$v.form_user.password.$touch()" @blur="$v.form_user.password.$touch()" label="Пароль"  ></v-text-field>
 <v-text-field v-model="form_user.password_r" :error-messages="passwordRepeatErrors" @input="$v.form_user.password_r.$touch()" @blur="$v.form_user.password_r.$touch()" required label="Пароль повторить"  ></v-text-field>
 <v-select v-model="form_user.role" :items="roles" label="Статус" ></v-select>
 <v-btn small :disabled="status" @click="add_user"> Добавить</v-btn>
 </v-form>
</v-card-text><v-card-actions>
          <v-spacer></v-spacer>
 <v-btn color="green darken-1"  text @click="dialog = false" > Закрыть</v-btn>
 </v-card-actions>
  </v-card>
  </v-dialog>

<h3>Users</h3>
<v-btn small color="red" @click="dialog=true">Add</v-btn>

<v-data-table :headers="headers" :items="users" :items-per-page="10" class="elevation-1" >
  <template v-slot:item.action="{ item }">
    <v-btn colro="red" small @click="del_user(item)">x</v-btn>
   </template>
</v-data-table>
 <v-spacer></v-spacer>
</v-container>
`,
    data: () => ({

          roles  : ['Admin','Moderator' ,'User'],
          dialog : false,
         form_user :{
               login : '',
               email : '',
               password : '',
               password_r : '',
               role :  'Moderator',
         },

        headers: [

            { text: 'Login', value: 'login' },
            { text: 'Email', value: 'email' },
            {text: 'Status', value: 'role'},
            {text: 'Actions', value: 'action'},

        ],


    }),
    validations:{
        form_user:{
             login:{
                 required,
                 minLength: minLength(4)
             },
            email :{
                required,
                email,
                minLength: minLength(6)
            },
            password: {
                required,
            },
            password_r: {
                required,
                sameAs: sameAs(function () {
                    return this.form_user.password
                })
            }

        }
    },
    computed: {
        list_users : function() {
            return this.$store.state.users
        },
        loginErrors() {
            const errors = []
            if (!this.$v.form_user.login.$dirty) return errors
            !this.$v.form_user.login.minLength && errors.push('Длина логина должна быть больше 4 символов')
            !this.$v.form_user.login.required && errors.push('Логин обязательно.')
            return errors
        },
        status : function(){
            return this.$v.form_user.$anyError

        },
        emailErrors(){
            const errors = []
            if (!this.$v.form_user.email.$dirty) return errors
            !this.$v.form_user.email.minLength && errors.push('Длина логина должна быть больше 6 символов')
            !this.$v.form_user.email.required && errors.push('Эмайл обязательно.')
            !this.$v.form_user.email.email && errors.push('Формат эмайла неправельный')
            return errors
        },
        passwordErrors(){
            const errors = []
            if (!this.$v.form_user.password.$dirty) return errors
            !this.$v.form_user.password.required && errors.push('Пароль обязательно.')
            return errors
        },
        passwordRepeatErrors(){
            const errors = []
            if (!this.$v.form_user.password_r.$dirty) return errors
            !this.$v.form_user.password_r.required && errors.push('Пароль обязательно.')
            !this.$v.form_user.password_r.sameAs && errors.push('Пароль пароли разные.')
            return errors
        },
        users: function(){
          return this.$store.state.users
        }
    },
    mounted() {
    this.$store.dispatch('all_users')
    },

    methods: {
        add_user(){
            this.$v.$touch()
            if(!this.$v.invalid){
                this.$store.dispatch('create_user',{user:this.form_user})
            }
          
        }
        ,
        del_user(user){

            this.$store.dispatch('del_user',{id:user.id})
        }

    }

}

