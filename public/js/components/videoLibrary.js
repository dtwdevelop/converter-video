Vue.use(window.vuelidate.default)
const { required, minLength, sameAs, email } = window.validators
export default {
    name : 'VideoLibrary',
    template : `
    <v-container>
<h3>Video list</h3>
    <v-dialog persistent v-model="dialog" width="600">
    <v-card>
    <v-card-title>
          <span class="headline">Transcode video</span>
        </v-card-title>
    <v-card-text>
    <v-alert v-if="alert" color="warning">Please fill all fields and select video</v-alert>
    <v-form  dense ref="form" >
  <v-row>
  <v-col sm="2">
   <v-text-field required  v-model="channel.name"  :error-messages="nameError" @input="$v.channel.name.$touch()" @blur="$v.channel.name.$touch()" label="Название"  ></v-text-field>
</v-col>
 <v-col sm="2">
 <v-file-input  v-model="channel.logo" prepend-icon="mdi-file"  :error-messages="fileError" @input="$v.channel.logo.$touch()" @blur="$v.channel.logo.$touch()" chips  label="Лого"></v-file-input>
</v-col>
</v-row>
<v-row>
<v-col sm="2"> <v-select v-model="channel.video_codecs" chips  :items="video_codecs" label="Видео кодекс" required></v-select></v-col>
<v-col sm="2"> <v-select v-model="channel.audio_codecs" chips :items="audio_codecs" label="Аудио кодекс" required></v-select></v-col>
<v-col sm="2"> <v-select v-model="channel.sizes" chips :items="sizes" label="Размеры" required></v-select></v-col>
<v-col sm="2"> <v-select v-model="channel.fps" chips :items="fps" label="Кадровая частота" required></v-select></v-col>
</v-row>
<v-row>
<v-col sm="2"><v-btn :disabled="loading" color="success" @click="convertVideo()" class="mr-4">Начать</v-btn></v-col>
<v-progress-circular v-if="loading" indeterminate color="primary"></v-progress-circular>
<v-progress-linear v-model="status" height="25">
      <strong>{{ Math.ceil(status) }}%</strong>
 </v-progress-linear>
</v-row>
</v-form>

</v-card-text>
 <v-card-actions>
 <v-btn @click="dialog=false">x</v-btn>
</v-card-actions>
</v-card>
</v-dialog>
 
    <v-btn to="/library" small>Back</v-btn>
    <v-data-table v-model="files"  show-select dense :headers="headers" :items="videos" :items-per-page="10" class="elevation-1" >
     <template v-slot:item.filename="{ item }">
     <video width="200" height="150" controls>
  <source :src="'/uploads/'+item.filename" type="video/mp4">
   Your browser does not support the video tag.
  </video>
     </template>
     <template v-slot:item.action="{item}">
     <v-btn small color="red"><v-icon color="white">mdi-delete</v-icon></v-btn>
   </template>
</v-data-table>
<v-spacer></v-spacer>
 <v-btn small @click="dialog=true">Transcode</v-btn>

   </v-container>
    `,
    data :()=>({
        dialog :false,
        alert:false,
        files : [],
        genres : [] ,
        sizes : [
            '720x576',
            '1280x729',
            '1920x1080',
            '3840x2160'
        ],
        fps : [400,1200,1500,4000,8000,14000],

        video_codecs : ['x264'],
        audio_codecs: ['aac','mp3','vorbis','flac'],
        channel :{
            name : '',
            logo : [],
            video_codecs : [],
            audio_codecs : [],
            sizes : [],
            fps : []
        },
        headers: [
            {text: 'Id', value: 'id'},
            { text: 'Video', value: 'filename' },
            { text: 'Actions', value: 'action' },

        ],
    }),
    validations :{
        channel :{
            name :{
                required,
                minLength :minLength(4)
            },
            logo :{
                required
            },
            video_codecs: {
                required
            },
            audio_codecs :{
                required
            },
            sizes:{
                required
            },
            fps:{
                required
            }
        }
    },
    computed :{
        videos : function(){
            return this.$store.state.videos
        },
        nameError(){
            const errors = []
            if (!this.$v.channel.name.$dirty) return errors
            !this.$v.channel.name.minLength && errors.push('Название  должна быть больше 4 символов')
            !this.$v.channel.name.required && errors.push('Название обязательно.')
            return errors
        },

        fileError(){
            const errors = []
            if (!this.$v.channel.logo.$dirty) return errors
            !this.$v.channel.logo.required && errors.push('Логотип обязательно.')
            return errors
        },
        selectErrors(){

        },
        loading : function(){
            return this.$store.state.loading
        },
        status : function(){
            return this.$store.state.convert_progress
        }

    },
    mounted(){
        const id = this.$route.params.id
        this.$store.dispatch('all_videos',{param:id})
    },
    methods :{
       convertVideo(){
            this.$v.$touch()
           if(this.$v.$invalid){
            this.alert=true
           }
           else{
               // this.$store.dispatch('convert_video',{params:this.channel, files: this.files[0].filename})
               this.$store.dispatch('convert_video',{params:this.channel, files:this.files[0].filename})
               this.$store.dispatch('progress_status')
           }
       }
    }
}
