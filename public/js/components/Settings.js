export default {
    name: 'Settings',
    template: `
<v-container>

<h3>Settings</h3>

<v-expansion-panels>

    <v-expansion-panel>
      <v-expansion-panel-header>
       Categories
      </v-expansion-panel-header>
      <v-expansion-panel-content>
 <v-row>
    <v-col >
      <v-data-table dense :headers="headers" :items="categories" :items-per-page="10" class="elevation-1" >
      <template v-slot:item.action="{ item }">
    <v-btn colro="red" small @click="deleteCategory(item.id)">x</v-btn>
   </template>
   <template v-slot:item.name="props">
        <v-edit-dialog :return-value.sync="props.item.name" @save="save" >
          {{ props.item.name }}
          <template v-slot:input>
            <v-text-field v-model="props.item.name" label="Change" single-line counter></v-text-field>
          </template>
        </v-edit-dialog>
      </template>
</v-data-table>
</v-col>
</v-row>
      <v-row>

      <v-form>
      <v-col sm="8">

       <v-text-field v-model="category" label="Category name" required ></v-text-field>

<!--        <v-select v-if="total" item-text="name" item-value="id" v-model="sub_category" :items="categories" label="Родительская " required></v-select>-->
          <v-btn small @click="addCategory"> Add </v-btn>
       </v-col>


       </v-form>
       </v-row>

      </v-expansion-panel-content>
    </v-expansion-panel>

      <v-expansion-panel>
      <v-expansion-panel-header>
       Genres
      </v-expansion-panel-header>
      <v-expansion-panel-content>
        <v-form>
       <v-data-table dense :headers="headers2" :items="genres" :items-per-page="10" class="elevation-1" >
      <template v-slot:item.action="{ item }">
    <v-btn colro="red" small @click="deleteGenre(item.id)">x</v-btn>
   </template>
       <template v-slot:item.name="props">
        <v-edit-dialog :return-value.sync="props.item.name" @save="save" >
          {{ props.item.name }}
          <template v-slot:input>
            <v-text-field v-model="props.item.name" label="Change" single-line counter></v-text-field>
          </template>
        </v-edit-dialog>
        </template>

       </v-data-table>

        <v-row>
      <v-col sm="8">
       <v-text-field v-model="genre" label="Name genre" required ></v-text-field>
        <v-btn small @click="addGenre"> Добавить </v-btn>
       </v-col>

       </v-row>
</v-form>
      </v-expansion-panel-content>
    </v-expansion-panel>

     <v-expansion-panel>
      <v-expansion-panel-header>
       Options
      </v-expansion-panel-header>
      <v-expansion-panel-content>
        Options
      </v-expansion-panel-content>
    </v-expansion-panel>

  </v-expansion-panels>
</v-container>`,
    data: () => ({
        category: null,
        sub_category: null,
        genre: null,
        headers: [

            {text: 'Name', value: 'name'},
            {text: 'Actions', value: 'action'},

        ],
        headers2: [

            {text: 'Name', value: 'name'},
            {text: 'Actions', value: 'action'},

        ],


    }),
    computed: {
        categories : function(){
            return this.$store.state.categories
        },

        genres : function(){
            return this.$store.state.genres
        },

        total : function(){
            return (this.$store.state.categories.length > 0)?true:false
        }

    },

    created() {
        this.$store.dispatch('get_categories')
        this.$store.dispatch('get_genres')

    },

    methods: {
        addCategory() {
           this.$store.dispatch('create_category',{name:this.category})
        },

        deleteCategory(i){

          this.$store.dispatch("delete_category",{id:i})
        },

        deleteGenre(genre){
            this.$store.dispatch("delete_genre",{id:genre})
        },

        addGenre() {
            this.$store.dispatch("create_genre",{name: this.genre})
        },
        save () {

        },




    }
}
