const Dashboard = () => import('./components/Dashboard.js')
const MediaLibrary = () => import('./components/mediaLibrary.js')
const Admin = () => import('./components/Admin.js')
const Channels  = () => import('./components/Channels.js')
const Settings  = () => import('./components/Settings.js')
const User   = () => import('./components/User.js')
const VideoLibrary =()=> import('./components/videoLibrary.js')
const Login = () =>import('./components/Login.js')

const routes = [
    { path: '/', component: Dashboard } ,
    { path: '/dashboard', component: Dashboard } ,
    { path: '/library', component: MediaLibrary } ,
    { path: '/channels', component: Channels} ,
    { path: '/settings', component: Settings} ,
    { path: '/users', component: User} ,
    {path: '/library/:id', component: VideoLibrary},
    {path: '/login', component: Login},

]
const router = new VueRouter({
    mode: 'history',
    routes:routes // short for `routes: routes`
})

if(document.querySelector("#app") !== null){
    new Vue({
        store ,
       // i18n,
        router,
        vuetify: new Vuetify(),
        render: h => h(Admin)
    }).$mount("#app")
}
