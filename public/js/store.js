const store = new Vuex.Store({
    state :{
        files : [],
        categories : [],
        genres : [],
        users : [],
        albums : [],
        videos :[],
        loading :false,
        token : null,
        users : [],
        progress : 0,
        convert_progress: 0,
    },

    getters:{

    },
    mutations:{
        /**
         * change files
         * @param {*} state 
         * @param {*} payload 
         */
        change_files(state, payload) {
            state.files  = payload.files
        },
        /**
         * change categories
         * @param {*} state 
         * @param {*} payload 
         */
        change_categories(state , payload){

           state.categories  = payload.category
        },
        /**
         * change genres
         * @param {*} state 
         * @param {*} payload 
         */
        change_genres(state,payload){
            const data_genres  = []
            state.genres  = payload.genre
        },
        /**
         * delete category
         * @param {*} state 
         * @param {*} payload 
         */
        del_category(state,payload){
            state.categories = state.categories.filter((item)=>{
                return item.id !== payload.id
            })
        },
        /**
         * delete genre
         * @param {*} state 
         * @param {*} payload 
         */
        del_genre(state,payload){
            state.genres = state.genres.filter((item)=>{
                return item.id !== payload.id
            })
        },
        /**
         * add user
         * @param {*} state 
         * @param {*} payload 
         */
        add_user(state,payload) {

            state.users = [...state.users ,payload.user]
        },
        /**
         * add user
         * @param {*} state 
         * @param {*} payload 
         */
        del_user(state,payload){
            state.users = state.users.filter((item)=>{
                return item.id !== payload.login
            })
        },
        /**
         * delete album
         * @param {*} state 
         * @param {*} payload 
         */
        del_album(state,payload){
            
           state.albums  = state.albums.filter((item)=>{
               return item.id !== payload.album_id
           });
        },
        /**
         * add album
         * @param {*} state 
         * @param {*} payload 
         */
        add_albums(state,payload){

            state.albums = payload.album
        },
        /**
         * add video
         * @param {*} state 
         * @param {*} payload 
         */
        add_videos(state,payload){
            state.videos  = payload.videos
        },
        /**
         * loading status
         * @param {*} state 
         * @param {*} payload 
         */
        loading_status(state,payload){
            state.loading =payload.loading
        },
        /**
         * change token
         * @param {*} state 
         * @param {*} payload 
         */

        change_token(state,payload){
            
           state.token = payload.token
        },
        /**
         * change all user
         * @param {*} state 
         * @param {*} payload 
         */
        change_all_users(state,payload){
            state.users  = payload.users
        },
        change_progress(state,payload){
         state.progress = payload.progress
        },
        change_progress_convert(state,payload){
            state.convert_progress  =payload.prog
        },

    },

    actions :{
        async  load_files({ commit, state },payload) {
            try {
                const res = await axios.get('/files')
                commit('change_files',{'files':res.data.files})


            } catch (err) {
                console.log("error " + err)
            }
        },

        async get_categories({commit,state},payload){
            try{
                const res = await axios.get('/get-categories')

                commit('change_categories',{category:res.data})
            }
            catch(err){
                console.log(err)
            }

        },

        async delete_category({commit,state},payload){
            try{
                const res = await axios.delete('/delete-category?id='+payload.id)
                commit("del_category",{id:payload.id})

            }
            catch(err){
                console.log(err)
            }

        },


        async get_genres({commit,state},payload){
            try{
                const res = await axios.get('/get-genres')
                commit('change_genres',{genre:res.data})
            }
            catch(err){
                console.log(err)
            }
        },

        async delete_genre({commit,state},payload){
            try{

                const res = await axios.delete('/delete-genre?id='+payload.id)
                commit('del_genre',{id:payload.id})
            }
            catch(err){
                console.log(err)
            }
        },

        async create_genre({commit,state,dispatch},payload){
            try{
                const res =  await axios.post('/add-genre',{name:payload.name ,hide:0})
                dispatch('get_genres')

            }
            catch(err){
                console.log(err)
            }
        },

        async create_category({commit,state,dispatch},payload){
            try{
               const res = await axios.post('/add-category',{name:payload.name})
               dispatch('get_categories')
            }
            catch(err){
                console.log(err)
            }
        },

        async create_user({commit,state},payload){
            try{
                commit('add_user',{user:payload.user})
                const res = await axios.post('/create-user',{user:payload.user})
            }
            catch (err) {
                console.log(err)
            }
            },

        async del_user({commit,state},payload){
            try{
                commit('del_user',{login:payload.id})
                const res  = axios.delete('/delete-user?id='+payload.id)
                console.log(res.data)
            }
            catch(err){
              console.log(err)
            }
        },
        async login_in({commit,state},payload){
            try{
                const res = await axios.post('/login',{login:payload.login,password:payload.password})
                commit('change_token',{token:res.data.token})
                if(state.token !=null){
                     return {login:'ok'}
                }
                return {login:'no'}
               
                }
            catch( err){
              console.log(err)
            }
        },

        async logout({state,commit},payload){
            try{
                const res  = await axios.post('/logout?token='+state.token)
                if(res.data.status ='logout') commit('change_token',{token:null})
            }
            catch(err){
                console.log(err)
            }
        },

        async add_library({commit,state,dispatch},payload){
            try{
                const config = {
                    headers :{'Content-Type': 'multipart/form-data' },
                    onUploadProgress: function(progressEvent) {
                      var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                      commit('change_progress',{progress:percentCompleted})
                      
                    }
                  }

                var bodyFormData = new FormData()
                bodyFormData.set('data', JSON.stringify(payload.data))
                const upfiles = []
                payload.files.forEach((f,index)=>{
                    bodyFormData.append('ff['+index+']',f);

                })

                const res = await axios.post('/add-library',bodyFormData,config)
            }
            catch(err){
                console.log(err)
            }
        },

        async get_all_albums({commit,state},payload){
            try{
                const res  = await axios.get('/albums');

                commit('add_albums',{album:res.data})
            }
            catch (err){
                console.log(err)
            }
            },

        async all_videos({commit,state},payload){
           try{
               const res = await  axios.get('/album/'+payload.param)
               commit('add_videos',{videos:res.data})
           }
           catch (err){
               console.log(err)
           }
        },

        async convert_video({commit,state},payload){
            try{
                commit('loading_status',{loading:true})
                const filename  = payload.files
                const res   = await  axios.post('/convert-video',{file:filename,params:payload.params})
            }
            catch(err){
                console.log(err)
            }
        },

         progress_status({commit,state},payload){
            const finished =  window.setInterval(async()=>{
                const load =  await axios.get('/loading')
              commit('change_progress_convert',{prog:load.data.progress})
              if(load.data.progress == 100){
                  commit('loading_status',{loading:false})
                  window.clearInterval(finished)
                  commit('change_progress_convert',{prog:0})
              }

            },1000)

        },

       async remove_album({commit,state},payload){
            try{
                commit('del_album',{album_id : payload.album_id})
                const res  = axios.delete('/remove-album?id='+payload.album_id)
                }
            catch(err){
                console.log(err)
            }
         },

        async all_users({state,commit},payload){
            try{
               const res  = await axios.get('/all-users')
               commit('change_all_users',{users:res.data})
            }
            catch(err){

            }

        }

   }


})
