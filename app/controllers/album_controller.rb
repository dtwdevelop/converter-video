require 'json'

class AlbumController < ApplicationController
  skip_before_action :verify_authenticity_token

  # empty file
  def files
    files = { :files => [] }
    render :json => files
  end

  def albums
    begin
      render :json => Album.all
    rescue ActionViewError => err
      render :json => err
    end
  end

  # one albums
  def one_album
    id = params[:id]
    albums = Album.select('file.id', 'file.filename').where("albums.id=?", id).joins("left join album_files as file  on file.album_id=albums.id")
    render :json => albums
  end

  # show progress status
  def loading
    val = Rails.cache.read('loading')
    render :json => { :progress => val }
  end

  # convert video
  def convert_video
    Rails.cache.write('loading', 0)
    filename = params[:file]
    data = params[:params]
    options = %W(-profile:v baseline -level 3.0 -ab 128 -b #{data['fps']} -s #{data[:sizes]} -start_number 0 -hls_time 2 -hls_list_size 0 -f hls)
    ConvertvJob.set(wait: 5.second).perform_later(filename, options)
    render :json => { :message => "finished convert"  }
  end

  #add album to db and save file
  def add_album
    @file_upload = ""
    data = JSON.parse(params[:data])
    album = Album.new(name: data['name'], category_id: ['categories'], genre_id: data['genres'], description: data['desc'])
    album.save
    files = params[:ff]
    files.each_value do |f|
      @file_upload = f
      album.album_files.create(album_id: album.id, filename: f.original_filename)
      File.open(Rails.root.join('public', 'uploads', f.original_filename), 'wb') do |file|
        file.write(f.read)
      end
    end
    render :json => { :message => 'file uploaded', :data => data['categories'] }
  end

  #sample action for testing
  def sample
    album = Album.find(4)
    files = album.album_files.all
    render :json => { :album => album, :files => files }
  end

  # get one album
  def get_album
    render :json => Album.find(params[:id])
  end

  #delete album and files
  def delete_album
    begin
      album = Album.find(params[:id])
      videos = album.album_files.all.each do |item|

        if (File.exist?(Rails.root.join('public', 'uploads', item.filename)))
          File.delete(Rails.root.join('public', 'uploads', item.filename))
        end
      end
      album.destroy
      render :json => { data: videos }

    rescue StandardError => err
      render :json => { message: err }
    end
  end
end
