require 'jwt'
require 'logger'
class UserController < ApplicationController
  skip_before_action :verify_authenticity_token
   # before_action :authenticate, except: [:user_all,:create_user]


  def user_all
    users =  User.select('id','login','email','role').all
    render :json => users
  end

  def login
     token_user = User.find_by(email: params[:email])
     status = token_user.authenticate(params[:password])
     if(token_user &&  status)
       render :json => {:token=>token_user.token }
     else
       render :json => {:token=>nil ,:pass=>password ,:status=>token_user}
     end
  end

  def logout
    user = User.find_by(token:params[:token])
    session[:logout] = user.id
  end

  def create_user
      password  = BCrypt::Password.create(params[:user]['password'])
      payload  = {data: params[:user]['login']}
      token = JWT::encode(payload,params[:password], 'HS256')
      user = User.new(login:params[:user]['login'], email:params[:user]['email'],
                      role:params[:user]['role'], password:params[:user]['password'],
                      password_confirmation:params[:password_r], token:token)
     if(user.valid?)
       user.save
     render :json=> {:msg=>'user created'}
     else
       render :json=> {:msg=>'user create failed'}

     end
  end

  def delete_user
    User.destroy(params[:id])
    render :json => {:msg=>'user deleted'}
  end

  def change_role
    begin
    new_role = params[:role]
    user = User.find_by(email:params[:email])
    user.role  = new_role.to_s
    user.save
    rescue  ActiveRecordError =>err
      render :json =>{:msg=>err}
    end
  end

  private
  def authenticate
    authenticate_or_request_with_http_token do |token, options |
      token_user = User.find_by(token:token)
      logger = Logger.new("log/token_check.txt")
      logger.info("token: #{token}")
       ActiveSupport::SecurityUtils.secure_compare(token, token_user.token)
    end
  end

end
