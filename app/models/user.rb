require 'bcrypt'
class User < ApplicationRecord
  has_secure_password validations: false
  validates :login , length: {minimum:4}
  validates :email , length:{minimum:5}
  validates :role , length:{minimum:5}
  validates :password, presence: true


  before_create do
    # self.password_digest = BCrypt::Password.create(self.password_digest)
  end

end
