require 'streamio-ffmpeg'
require 'logger'
class ConvertvJob < ApplicationJob
  queue_as :default

  def perform(filename,options)
    # Do something later
    logger = Logger.new("log/convert_log.txt")
    begin
    Rails.cache.clear
    url = Rails.root.join('public', 'uploads', filename)
    url_transcoder   = Rails.root.join('public', 'transcoder','out.m3u8')
    movie = FFMPEG::Movie.new("#{url}")
    logger.info('start transcode')
    movie.transcode("#{url_transcoder}" ,options) do |progress|
      percentage_value  = progress * 100
      pros = percentage_value.to_i
      logger.info("Total procent left: #{pros.to_s}")
      Rails.cache.write('loading', pros)
    end
    logger.info('finished transcode')
    rescue StandardError => err
      logger.error(err)
    end
  end
end
